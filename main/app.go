package main

import "fmt"

func main() {
	fmt.Println(longestCommonPrefix([]string{"flower", "flow", "flight"}))
	fmt.Println(longestCommonPrefix([]string{"dog", "racecar", "car"}))
	fmt.Println(longestCommonPrefix([]string{"a"}))
}

func longestCommonPrefix(strs []string) string {
	base, result := strs[0], ""

	if len(strs) > 1 {
		for index, char := range base {
			insert := true

			for i := 1; i < len(strs); i++ {
				if index >= len(strs[i]) || char != rune(strs[i][index]) {
					insert = false

					break
				}
			}

			if insert {
				result += string(base[index])
			} else {
				break
			}
		}

		return result
	} else {
		return base
	}
}
